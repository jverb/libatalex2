﻿using System;
using System.Collections.Generic;

namespace Alexandria.Data
{
    public class Result : BaseObject
    {

        public DateTime Date { get; set; }
        public string Score { get; set; }
        public Guid GameId { get; set; }
        public Game Game { get; set; }
        public virtual ICollection<UserResult> Players { get; set; }
    }
}
