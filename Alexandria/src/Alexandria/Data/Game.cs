﻿using Alexandria.Models;
using System.Collections.Generic;

namespace Alexandria.Data
{
    public class Game : BaseObject
    {
        public string Title { get; set; }
        public int MinPlayers { get; set; }
        public int MaxPlayers { get; set; }
        public int PlayTimeMinutes { get; set; }
        public int MinimumAgeYears { get; set; }
        public bool IsOnShelf { get; set; }

        public string BGG_ID { get; set; }
        public string BGG_Family { get; set; }
        public string BGG_ImageUrl { get; set; }
        public float BGG_Rating { get; set; }
        public int BGG_SuggestedPlayer { get; set; }
        public string BGG_ThumbUrl { get; set; }
        public int BGG_YearPublished { get; set; }

        public ICollection<Borrow> Borrows { get; set; }
        public ICollection<Result> Results { get; set; }
        public ICollection<UserGame> Owners { get; set; }
    }
}
