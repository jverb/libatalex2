﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Alexandria.Data
{
    public class BaseObject
    {
        [Key]
        public Guid Id { get; set; }

        public DateTime CreatedAt { get; set; }        

    }
}
