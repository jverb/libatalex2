﻿using Alexandria.Models;
using System;

namespace Alexandria.Data
{
    public class UserResult : BaseObject
    {
        public bool IsWinner { get; set; }
        public string UserId { get; set; }
        public ApplicationUser User { get; set; }
        public Guid ResultId { get; set; }
        public Result Result { get; set; }
    }
}
