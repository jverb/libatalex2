﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Alexandria.Data.Migrations
{
    public partial class AddedBGGPropsToGame : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "BGG_Family",
                table: "Games",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "BGG_ID",
                table: "Games",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "BGG_ImageUrl",
                table: "Games",
                nullable: true);

            migrationBuilder.AddColumn<float>(
                name: "BGG_Rating",
                table: "Games",
                nullable: false,
                defaultValue: 0f);

            migrationBuilder.AddColumn<int>(
                name: "BGG_SuggestedPlayer",
                table: "Games",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "BGG_ThumbUrl",
                table: "Games",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "BGG_YearPublished",
                table: "Games",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "BGG_Family",
                table: "Games");

            migrationBuilder.DropColumn(
                name: "BGG_ID",
                table: "Games");

            migrationBuilder.DropColumn(
                name: "BGG_ImageUrl",
                table: "Games");

            migrationBuilder.DropColumn(
                name: "BGG_Rating",
                table: "Games");

            migrationBuilder.DropColumn(
                name: "BGG_SuggestedPlayer",
                table: "Games");

            migrationBuilder.DropColumn(
                name: "BGG_ThumbUrl",
                table: "Games");

            migrationBuilder.DropColumn(
                name: "BGG_YearPublished",
                table: "Games");
        }
    }
}
