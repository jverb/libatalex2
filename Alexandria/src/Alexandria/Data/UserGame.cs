﻿using Alexandria.Models;
using System;

namespace Alexandria.Data
{
    public class UserGame : BaseObject
    {
        public string UserId { get; set; }
        public ApplicationUser User { get; set; }
        public Guid GameId { get; set; }
        public Game Game { get; set; }
    }
}
