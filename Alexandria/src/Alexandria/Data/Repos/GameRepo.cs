﻿using Alexandria.Data;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace Data.Repositories
{
    public class GameRepo:BaseRepo
    {
        public IQueryable<Game> GetAll()
        {
            var db = new ApplicationDbContext(new DbContextOptions<ApplicationDbContext> { });
            return db.Games;
        }

        public Game Get(Guid Id)
        {
            return GetAll().FirstOrDefault(g => g.Id == Id);
        }

        public bool IsLentOut(Guid Id)
        {
            return IsLentOut(Get(Id));
        }

        public bool IsLentOut(Game game)
        {
            return game.Borrows.Any(b => b.DateIn == null);
        }

        public bool IsBeingPlayed(Guid Id)
        {
            var game = Get(Id);
            return (!game.IsOnShelf && !IsLentOut(game));
        }

        public void LendOut(Guid Id, string borrowerName)
        {
            var db = new ApplicationDbContext(new DbContextOptions<ApplicationDbContext> { }); //GetContext<ApplicationDbContext>();
            var game = db.Games.FirstOrDefault(g => g.Id == Id);
            if(game == null || !game.IsOnShelf)
            {
                return;
            }

            game.IsOnShelf = false;
            Borrow newBorrow = new Borrow {DateOut = DateTime.UtcNow, Name = borrowerName, Game = game };
            db.Borrows.Add(newBorrow);
            db.SaveChanges();
        }

        public bool Play(Guid Id)
        {
            var db = new ApplicationDbContext(new DbContextOptions<ApplicationDbContext> { }); //GetContext<ApplicationDbContext>();
            var game = db.Games.FirstOrDefault(g => g.Id == Id);
            if (game == null || !game.IsOnShelf)
            {
                return false;
            }

            game.IsOnShelf = false;
            db.SaveChanges();
            return true;
        }

        public bool CheckIn(Guid Id)
        {
            var db = new ApplicationDbContext(new DbContextOptions<ApplicationDbContext> { }); //GetContext<ApplicationDbContext>();
            var game = db.Games.Include(g=>g.Borrows).FirstOrDefault(g => g.Id == Id);
            if (game == null || game.IsOnShelf || !game.Borrows.Any(b=>b.DateIn == null))
            {
                return false;
            }
            var borrow = game.Borrows.FirstOrDefault(b => b.DateIn == null);
            borrow.DateIn = DateTime.UtcNow;
            game.IsOnShelf = true;
            db.SaveChanges();
            return true;
        }
    }
}
