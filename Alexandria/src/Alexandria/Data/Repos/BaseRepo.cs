﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;

namespace Data.Repositories
{
    public abstract class BaseRepo : IDisposable
    {
        /// <summary>
        /// Provides lifetime management for DB contexts withing scope of current repo
        /// </summary>
        protected Dictionary<Type, DbContext> Contexts = new Dictionary<Type, DbContext>();

        /// <summary>
        /// Provides lifetime management for DB contexts within scope of current repo
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        protected T GetContext<T>() where T : DbContext, new()
        {
            DbContext context = null;

            if (!Contexts.TryGetValue(typeof(T), out context))
            {
                context = new T();
                Contexts.Add(typeof(T), context);
            }

            return (T)context;
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    if (Contexts != null)
                    {
                        foreach (var context in Contexts)
                        {
                            if (context.Value != null)
                            {
                                context.Value.Dispose();
                            }
                        }
                    }
                }

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~SystemAttachmentsRepo() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        void IDisposable.Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion        
    }
}
