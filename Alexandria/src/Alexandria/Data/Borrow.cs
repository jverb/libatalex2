﻿using System;

namespace Alexandria.Data
{
    public class Borrow : BaseObject
    {
        public string Name { get; set; }
        public DateTime DateOut { get; set; }
        public DateTime? DateIn { get; set; }
        public Guid GameId { get; set; }
        public Game Game { get; set; }
    }
}
