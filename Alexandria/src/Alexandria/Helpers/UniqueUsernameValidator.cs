﻿using Alexandria.Data;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Alexandria.Helpers
{
    public class UniqueUsernameValidator : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            ApplicationDbContext db = validationContext.GetService(typeof(ApplicationDbContext)) as ApplicationDbContext; // Ok that's pretty cool
            if (db.Users.Any(u => u.UserName == value.ToString()))
            {
                return new ValidationResult($"{value} is not available.");
            }
            else
            {
                return null;
            }

        }
    }
}
