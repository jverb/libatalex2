﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace libatalex.Models
{
    public class LendViewModel
    {
        public Guid GameId { get; set; }
        [Display(Name="Name")]
        public string BorrowerName { get; set; }
    }
}
