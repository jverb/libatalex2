﻿using Alexandria.Data;
using Alexandria.Models.AccountViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Alexandria.Models.HomeViewModels
{
    public class HomeIndexViewModel
    {
        public List<Game> Games { get; set; }
        public int NumberOfPlayers { get; set; }
        public int NumberOfMinutes { get; set; }
        public Guid GameId { get; set; }
        public RegisterViewModel RegisterVM { get; set; }
    }
}
