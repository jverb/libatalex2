﻿"use strict";

$(document).ready(function () {

    $("#gameSearch").keyup(function (e) {
        BGG_Search($(this).val(), function (results) {
            var content = "";
            $(results).find('item').each(function () {
                content = content.concat("<span>" + $(this).find('name').attr('value') + "</span>");
            });
            $("#searchResults").html(content);
        });
    });
});

