﻿var BGG_BASE_URL = "https://www.boardgamegeek.com/xmlapi2/";
var BGG_GAME_SEARCH = "search?type=boardgame,boardgameexpansion&query=";
var BGG_GAME_DETAILS = "thing?stats=1&id=";
var BGG_QUEUED_RESPONSE = "Please try again later for access";
var WAIT_TIME = 5000;
function BGG_Search(searchTerm, callback)
{
    $.ajax({
        url: BGG_BASE_URL + BGG_GAME_SEARCH + encodeURI(searchTerm),
        cache: false,
        dataType: 'text',
        type: "GET",
        crossDomain: true,
        useCorsAnywhere:true,
        xhrFields: { withCredentials: false },
        headers:{},
        success: function (data) {
            var xmlData = $.parseXML(data.responseText);
            if (data.responseText.indexOf(BGG_QUEUED_RESPONSE) > -1) {
                setTimeout(function () {
                    SearchBGG(searchTerm);
                }, WAIT_TIME);

            } else if ($(xmlData).find('item').length > 0) {
                callback(xmlData);
            } 
        },
        error: function (e) {
            alert("something blew up: " + e);
        }
    });
}

function BGG_GameDetails(id, callback)
{
    $.ajax({
        url: BGG_BASE_URL + BGG_GAME_DETAILS + encodeURI(id),
        cache: false,
        dataType: 'text',
        type: "GET",
        success: function (data) {
            var xmlData = $.parseXML(data.responseText);
            if (data.responseText.indexOf(BGG_QUEUED_RESPONSE) > -1) {
                setTimeout(function () {
                    BGG_GameDetails(id);
                }, WAIT_TIME);

            } else if ($(xmlData).find('item').length > 0) {
                callback(xmlData);
            }
        },
        error: function (e) {
            alert("something blew up: " + e);
        }
    });
}