﻿using Alexandria.Data;
using Alexandria.Models;
using Data.Repositories;
using libatalex.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Xml;

namespace Alexandria.Controllers
{
    [RequireHttps]
    public class GamesController : Controller
    {
        private readonly ApplicationDbContext db;

        public GamesController(ApplicationDbContext _db)
        {
            db = _db;
        }
        // GET: Games
        public ActionResult Index()
        {
            //UpdateGameInfo();
            //db.Games.Where(g=>g.Owners.Any(o=>o.UserId == User))
            var userManager = HttpContext.RequestServices.GetService(typeof(UserManager<ApplicationUser>)) as UserManager<ApplicationUser>;
            var userId = userManager.GetUserId(User);
            return View(db.Games.Where(g => g.Owners.Any(o => o.UserId == userId)).ToList());
        }

        private void UpdateGameInfo()
        {
            var games = db.Games.ToList().OrderBy(g => g.Title);
            foreach (var game in games)
            {
                game.Title = game.Title.Trim();
                db.SaveChanges();

                if (game.MaxPlayers != 0)
                {
                    continue;
                }
                var objectId = GetObjectId(game.Title);
                if (objectId != null)
                {
                    var values = GetDetails(objectId);
                    game.MinimumAgeYears = int.Parse(values["age"]);
                    game.MaxPlayers = int.Parse(values["maxplayers"]);
                    game.PlayTimeMinutes = int.Parse(values["playingtime"]);
                    game.MinPlayers = int.Parse(values["minplayers"]);
                    db.SaveChanges();
                    Thread.Sleep(1000);
                }
            }
        }

        private string GetObjectId(string name)
        {
            var searchURI = "http://www.boardgamegeek.com/xmlapi/search";

            string uriSuffix = ("?search=[GAMENAME]").Replace("[GAMENAME]", name.Trim(' '));
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(searchURI);
            client.DefaultRequestHeaders.Accept.Add(
            new MediaTypeWithQualityHeaderValue("application/xml"));

            byte[] dataObjects = null;
            client.GetByteArrayAsync(uriSuffix).ContinueWith(task =>
            {
                var foo = name;
                dataObjects = task.Result;
            }).Wait();
            string xmlResponse = System.Text.Encoding.UTF8.GetString(dataObjects);

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(xmlResponse);
            var element = xmlDoc.GetElementsByTagName("boardgame")[0];
            if (element != null)
            {
                var objectId = element.Attributes["objectid"];
                return objectId.Value;
            }
            return null;
        }

        private Dictionary<string, string> GetDetails(string objectId)
        {
            var gameURI = "http://www.boardgamegeek.com/xmlapi/boardgame/[GAMEID]";

            string completeUri = gameURI.Replace("[GAMEID]", objectId);
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(completeUri);
            client.DefaultRequestHeaders.Accept.Add(
            new MediaTypeWithQualityHeaderValue("application/xml"));

            byte[] dataObjects = null;
            client.GetByteArrayAsync("").ContinueWith(task =>
            {
                var foo = objectId;
                dataObjects = task.Result;
            }).Wait();
            string xmlResponse = System.Text.Encoding.UTF8.GetString(dataObjects);

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(xmlResponse);
            var element = xmlDoc.GetElementsByTagName("minplayers")[0];
            var minplayers = element.InnerText;
            element = xmlDoc.GetElementsByTagName("maxplayers")[0];
            var maxplayers = element.InnerText;
            element = xmlDoc.GetElementsByTagName("playingtime")[0];
            var playingtime = element.InnerText;
            element = xmlDoc.GetElementsByTagName("age")[0];
            var age = element.InnerText;
            var res = new Dictionary<string, string>();
            res.Add("minplayers", minplayers);
            res.Add("maxplayers", maxplayers);
            res.Add("playingtime", playingtime);
            res.Add("age", age);
            return res;
        }

        // GET: Games/Details/5
        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return BadRequest();
            }
            Game game = db.Games.FirstOrDefault(g => g.Id == id);
            if (game == null)
            {
                return NotFound();
            }
            return View(game);
        }

        public ActionResult Add()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Add(Game game)
        {
            if (ModelState.IsValid)
            {
                return Redirect("Index");
            }
            return View(game);
        }
        // GET: Games/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Games/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(include: "Id,Title,NumberOfPlayers,PlayTimeMinutes,MinimumAgeYears,IsOnShelf")] Game game)
        {
            if (ModelState.IsValid)
            {
                game.Id = Guid.NewGuid();
                db.Games.Add(game);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(game);
        }

        // GET: Games/Edit/5
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return BadRequest();
            }
            Game game = db.Games.FirstOrDefault(g => g.Id == id);
            if (game == null)
            {
                return NotFound();
            }
            return View(game);
        }

        // POST: Games/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(include: "Id,Title,NumberOfPlayers,PlayTimeMinutes,MinimumAgeYears,IsOnShelf")] Game game)
        {
            if (ModelState.IsValid)
            {
                db.Entry(game).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(game);
        }

        // GET: Games/Delete/5
        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return BadRequest();
            }
            Game game = db.Games.FirstOrDefault(g => g.Id == id);
            if (game == null)
            {
                return NotFound();
            }
            return View(game);
        }

        // POST: Games/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            Game game = db.Games.FirstOrDefault(g => g.Id == id);
            db.Games.Remove(game);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult Play(Guid id)
        {
            using (var repo = new GameRepo())
            {
                if (repo.Play(id))
                {
                    return PartialView("_PlaySuccess");
                }
                else
                {
                    return PartialView("_PlayFailure");
                }
            }
        }

        [HttpPost]
        public ActionResult CheckIn(Guid id)
        {
            using (var repo = new GameRepo())
            {
                if (repo.CheckIn(id))
                {
                    return PartialView("_CheckInSuccess");
                }
                else
                {
                    return PartialView("_CheckInFailure");
                }
            }
        }

        public ActionResult Lend(Guid id)
        {
            var vm = new LendViewModel();
            vm.GameId = id;
            return PartialView("_Lend", vm);
        }

        [HttpPost]
        public ActionResult Lend(LendViewModel vm)
        {
            using (var repo = new GameRepo())
            {
                repo.LendOut(vm.GameId, vm.BorrowerName);
            }
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
