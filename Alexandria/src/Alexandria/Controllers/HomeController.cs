﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Alexandria.Data;
using Alexandria.Models;
using Microsoft.AspNetCore.Identity;
using Alexandria.Models.HomeViewModels;
using Alexandria.Models.AccountViewModels;

namespace Alexandria.Controllers
{
    [RequireHttps]
    public class HomeController : Controller
    {
        private readonly ApplicationDbContext db;

        public HomeController( ApplicationDbContext _db)
        {
            db = _db;
        }

        public IActionResult Index()
        {
            var userManager = HttpContext.RequestServices.GetService(typeof(UserManager<ApplicationUser>)) as UserManager<ApplicationUser>;
            var userId = userManager.GetUserId(User);
            var vm = new HomeIndexViewModel
            {
                Games = db.Games.Where(g => g.Owners.Any(o => o.UserId == userId)).ToList()
            };

            vm.RegisterVM = new RegisterViewModel();
            return View(vm);
        }

        [HttpPost]
        public IActionResult Index(HomeIndexViewModel vm)
        {
            return RedirectToAction("Details", "Game", new { id = vm.GameId });
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View();
        }
    }
}
